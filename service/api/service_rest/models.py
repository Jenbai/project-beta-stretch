from django.db import models


class AutomobileVO(models.Model):
    import_href = models.CharField(max_length=200, unique=True)
    vin = models.CharField(max_length=17, unique=True)

    def __str__(self):
        return self.vin


class Technician(models.Model):
    name = models.CharField(max_length=200)
    employee_number = models.CharField(max_length=5, unique=True)

    def __str__(self):
        return self.name


class Appointment(models.Model):
    vin = models.CharField(max_length=17)
    name = models.CharField(max_length=200)
    date = models.DateField()
    time = models.TimeField()
    reason = models.TextField()

    technician = models.ForeignKey(
        Technician,
        related_name="appointments",
        on_delete=models.PROTECT
    )

    finished = models.BooleanField(default=False)

    def __str__(self):
        return self.name
