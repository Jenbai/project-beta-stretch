from django.urls import path

from .views import api_sales, api_sale, api_customers, api_customer, api_employees, api_employee, api_employee_sales

urlpatterns = [
    path("sales/", api_sales, name="api_sales"),
    path("sales/<int:id>", api_sale, name="api_sale"),
    path("customers/", api_customers, name="api_customers"),
    path("customers/<int:id>", api_customer, name="api_customer"),
    path("employees/", api_employees, name="api_employees"),
    path("employees/<int:id>", api_employee, name="api_employee"),
    path("employees/sales/<int:id>", api_employee_sales, name="api_employee_sales"),
]
