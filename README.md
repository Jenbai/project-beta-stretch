# CarCar

Team:

* Jen Bailey - Service microservice
* Diane Ancheril - Sales microservice

## Design

## Service microservice

Firstly I will add my app to the installed apps in the service project settings file.

I will create three models:
1. One for Technician which will contain a name field and employee number field, both CharField.
2. One for Appointment with vin as a CharField, name as a CharField, date as DateField, time as TimeField, reason as a TextField, technician as a ForeignKey with on delete set to Protect and another field for finished which will be a BooleanField with the default set to false.
3. I will create a final model for the Automobiles virtual object with fields only for the import_href and the vin number.

I will create an encoder for each of my models in a separate encoders file.

I will create a poller function called get automobiles to poll for automobile data from the inventory app to populate the automobile virtual object in the service app.

I will create view functions for two of my models for each CRUD method, GET, POST, GET(detail), PUT and DELETE. For my automobile vo I will create a view function with just the GET method.

I will create an api urls file and create paths for each view function.

I will update the urls file in service project to include all the paths from my api urls file.

I will create separate javascript files for appointments list, make an appointment and appointments history for my front end react pages. Each of these files will contain a class component.
1. For my appointment list page I will make an api call to fetch the data from my service app, I will then map through this data to populate the fields in my list. I will check to see if the vin is included in the automobile virtual object data and if it is VIP will be added to the page. I will have a delete button that deletes that appointment and a finished button that will PUT the finished field as true. If the appointment finished field is true it will not be rendered on the page.
2. For my create appointment page I will use a form to record an input change and then return a json response with a POST api call on submit.
3. For my appointments history I will make another api call to fetch the appointments data, I will then have a search form that includes an input area for the user to type in their vin. When the search button is clicked I will set the search vin to the state, I will then filter through the appointments data to find only the appointments with the search vin and render those on the page.

I will add my links to a route with browser routes in the app file and in the nav file I will create a dropdown nav bar using those paths.

Diane and I will split the inventory front end pages with Diane doing the models list, manufacturers list and add a manufacturer and I will do the model form, add an automobile and automobiles list pages.

## Sales microservice

I will be creating the following models:
    1. AutomobileVO: to access data in the Automobile model in the Inventory microservice.
    2. Employee model: to hold employee information (name and number)
    3. Customer model: to hold customer information (name, address and phone number)
    4. Sale model: to store the price of a sale, along with foreign keys to the employee model, the customer model and the AutomobileVO.

I will create the following view functions:
    1. api_sales: It will handle GET requests to list all sale records, and POST requests to record new sales.
    2. api_sale: It will handle GET requests to provide details of a single sale instance and PUT requests to allow the user to edit a sale instance. It will also handle DELETE requests.
    3. api_employees: It will handle GET and POST requests to list and add a sale record.
    4. api_employee: It will handle GET and PUT requests to show the details of a registered employee and edit an employee instance.
    5. api_employee_sales: It will render all of the sales linked to an employee.
    6. api_customers: It will handle GET and POST requests to list and add customer information.
    7. api_customer: It will handle GET and PUT requests to show and edit a single customer instance.

Once the view functions have been registered in urls.py, I will define the encoders which will render the data in JSON.

I will also set up a poller to poll for data from the Automobile model in the Inventory microservice and save it in AutomobileVO so that the sales model now has access to the vin from the value object.

On my front end, I will have the following components:
    1. A form to add a manufacturer by specifying the name.
    2. A page to list all of the registered manufacturers.
    3. A page to list all of the vehicle models with the manufacturer name and an image.
    4. A form to add customer information (name, phone number and address).
    5. A form to add employee information (name and number).
    6. A form to add a sale record. It makes use of drop-down lists which will only display automobiles that have not yet been sold, along with customers and employees who are already registered in the database.
    7. A page to list the details associated with all recorded sales.
    8. A page to list the sale history of any employee that is selected from a drop-down list containing all of the employees.
