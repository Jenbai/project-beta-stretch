import React from "react";

class SalesForm extends React.Component {
  state = {
    autos: [],
    employees: [],
    customers: [],
    sales: [],
    price: "",
    isAvailable: true,
  };

  handleInputChange = (e) => {
    const name = e.target.name;
    const value = e.target.value;
    this.setState({ [name]: value });
  };

  handleSubmit = async (e) => {
    e.preventDefault();

    const salesUrl = "http://localhost:8090/api/sales/";

    const fetchConfig = {
      method: "POST",
      body: JSON.stringify({
        customer: this.state.customer,
        vin: this.state.auto,
        price: this.state.price,
        employee: this.state.employee,
      }),
      headers: {
        "Content-Type": "application/json",
      },
    };

    const response = await fetch(salesUrl, fetchConfig);
    if (response.ok) {
      const cleared = {
        auto: "",
        employee: "",
        customer: "",
        price: "",
      };
      this.setState(cleared);
      this.loadData();
    }
  };

  loadData = async () => {
    const responseAuto = await fetch("http://localhost:8100/api/automobiles/");
    if (responseAuto.ok) {
      const data = await responseAuto.json();
      this.setState({ autos: data.autos });
    }

    const responseEmployee = await fetch(
      "http://localhost:8090/api/employees/"
    );
    if (responseEmployee.ok) {
      const data = await responseEmployee.json();
      this.setState({ employees: data.employees });
    }

    const responseCustomer = await fetch("http://localhost:8090/api/customers");
    if (responseCustomer.ok) {
      const data = await responseCustomer.json();
      this.setState({ customers: data.customers });
    }

    const responseSales = await fetch("http://localhost:8090/api/sales");
    if (responseSales.ok) {
      const data = await responseSales.json();
      this.setState({ sales: data.sales });
    }
  };

  async componentDidMount() {
    this.loadData();
  }

  render() {
    return (
      <div className="row">
        <div className="offset-3 col-6">
          <div className="shadow p-4 mt-4">
            <h1>Record a new sale</h1>
            <form onSubmit={this.handleSubmit} id="create-sale-form">
              <div className="mb-3">
                <select
                  onChange={this.handleInputChange}
                  name="auto"
                  id="autos"
                  value={this.state.auto}
                  className="form-select"
                >
                  <option>Choose an automobile</option>
                  {this.state.autos.map((auto) => {
                    for (let sale of this.state.sales) {
                      if (auto.vin === sale["vin"]) {
                        this.isAvailable = false;
                        break;
                      } else {
                        this.isAvailable = true;
                      }
                    }
                    if (
                      this.isAvailable === true ||
                      this.state.sales.length === 0
                    ) {
                      return (
                        <option key={auto.id} value={auto.vin}>
                          {auto.vin}
                        </option>
                      );
                    }
                  })}
                </select>
              </div>
              <div className="mb-3">
                <select
                  onChange={this.handleInputChange}
                  name="employee"
                  id="employees"
                  value={this.state.employee}
                  className="form-select"
                >
                  <option>Choose a sales person</option>
                  {this.state.employees?.map((employee) => {
                    return (
                      <option key={employee.id} value={employee.employee_name}>
                        {employee.employee_name} / {employee.employee_number}
                      </option>
                    );
                  })}
                </select>
              </div>
              <div className="mb-3">
                <select
                  onChange={this.handleInputChange}
                  name="customer"
                  id="customers"
                  value={this.state.customer}
                  className="form-select"
                >
                  <option>Choose a customer</option>
                  {this.state.customers?.map((customer) => {
                    return (
                      <option key={customer.id} value={customer.customer_name}>
                        {customer.customer_name} / {customer.phone_number}
                      </option>
                    );
                  })}
                </select>
              </div>
              <div className="input-group mb-3">
                <div className="input-group-prepend">
                  <span className="input-group-text">$</span>
                </div>
                <input
                  onChange={this.handleInputChange}
                  type="text"
                  name="price"
                  value={this.state.price}
                  className="form-control"
                  aria-label="Amount (to the nearest dollar)"
                />
                <div className="input-group-append">
                  <span className="input-group-text">.00</span>
                </div>
              </div>
              <button className="btn btn-primary">Create</button>
            </form>
          </div>
        </div>
      </div>
    );
  }
}

export default SalesForm;
