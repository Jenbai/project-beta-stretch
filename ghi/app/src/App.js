import { BrowserRouter, Routes, Route } from "react-router-dom";
import MainPage from "./MainPage";
import Nav from "./Nav";
import ManufacturersList from "./ManufacturersList";
import ManufacturerForm from "./ManufacturerForm";
import ModelForm from "./ModelForm";
import ModelsList from "./ModelsList";
import AutomobileForm from "./AutomobileForm";
import AutomobilesList from "./AutomobilesList";
import SalesList from "./SalesList";
import SalesForm from "./SalesForm";
import SalesPersonForm from "./SalesPersonForm";
import SalesPersonHistory from "./SalesPersonHistory";
import CustomerForm from "./CustomerForm";
import TechnicianForm from "./TechnicianForm";
import AppointmentsHistory from "./AppointmentsHistory";
import AppointmentForm from "./AppointmentForm";
import AppointmentsList from "./AppointmentsList";

function App() {
  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route path="/" element={<MainPage />} />
          <Route path="manufacturers">
            <Route path="new" element={<ManufacturerForm />} />
            <Route path="" element={<ManufacturersList />} />
          </Route>
          <Route path="models">
            <Route path="new" element={<ModelForm />} />
            <Route path="" element={<ModelsList />} />
          </Route>
          <Route path="automobiles">
            <Route path="new" element={<AutomobileForm />} />
            <Route path="" element={<AutomobilesList />} />
          </Route>
          <Route path="sales">
            <Route path="" element={<SalesList />} />
            <Route path="new" element={<SalesForm />} />
          </Route>
          <Route path="salesperson">
            <Route path="" element={<SalesPersonForm />} />
            <Route path="history" element={<SalesPersonHistory />} />
          </Route>
          <Route path="customer" element={<CustomerForm />} />
          <Route path="technician" element={<TechnicianForm />} />
          <Route path="appointments">
            <Route path="" element={<AppointmentsList />} />
            <Route path="new" element={<AppointmentForm />} />
            <Route path="history" element={<AppointmentsHistory />} />
          </Route>
        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;
