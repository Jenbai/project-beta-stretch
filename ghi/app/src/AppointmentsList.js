import React from "react";
import moment from "moment";

class AppointmentsList extends React.Component {
  state = {
    appointments: [],
  };

  loadData = async () => {
    const url = "http://localhost:8080/api/appointments/";
    const response = await fetch(url);
    if (response.ok) {
      const data = await response.json();
      this.setState({ appointments: data.appointments });
    }
    const autoUrl = "http://localhost:8080/api/automobiles/";
    const answer = await fetch(autoUrl);
    if (answer.ok) {
      const content = await answer.json();
      this.setState({ autos: content.autos });
    }
  };

  handleDelete = async (event) => {
    const id = event.target.id;
    await fetch(`http://localhost:8080/api/appointments/${id}/`, {
      method: "DELETE",
    });
    this.loadData();
  };

  handleFinish = async (event) => {
    const id = event.target.id;
    const fetchConfig = {
      method: "put",
      body: JSON.stringify({ finished: true }),
      headers: {
        "Content-Type": "application/json",
      },
    };
    const response = await fetch(
      `http://localhost:8080/api/appointments/${id}/`,
      fetchConfig
    );
    if (response.ok) {
      await response.json();
      this.loadData();
    }
  };

  async componentDidMount() {
    this.loadData();
  }

  render() {
    return (
      <div>
        <h1 className="header mt-4">Appointments List</h1>
        <table className="table mt-4">
          <thead>
            <tr>
              <th>VIN</th>
              <th>Customer Name</th>
              <th>Date</th>
              <th>Time</th>
              <th>Technician</th>
              <th>Reason</th>
              <th>Status</th>
            </tr>
          </thead>
          <tbody>
            {this.state.appointments.map((appointment) => {
              if (appointment.finished === false) {
                return (
                  <tr key={appointment.id} name="finish">
                    <td>{appointment.vin}</td>
                    <td>{appointment.name}</td>
                    <td>{appointment.date}</td>
                    <td>
                      {moment(appointment.time, "HH:mm:ss").format("h:mm a")}
                    </td>
                    <td>{appointment.technician.name}</td>
                    <td>{appointment.reason}</td>
                    <td>
                      {this.state.autos?.map((auto) => {
                        if (auto.vin === appointment.vin) {
                          return "VIP";
                        } else {
                          return "";
                        }
                      })}
                    </td>
                    <td>
                      <button
                        onClick={this.handleFinish}
                        style={{ backgroundColor: "green" }}
                        id={appointment.id}
                      >
                        Finished
                      </button>
                      <button
                        onClick={this.handleDelete}
                        style={{ backgroundColor: "red" }}
                        id={appointment.id}
                      >
                        Cancel
                      </button>
                    </td>
                  </tr>
                );
              }
            })}
          </tbody>
        </table>
      </div>
    );
  }
}
export default AppointmentsList;
