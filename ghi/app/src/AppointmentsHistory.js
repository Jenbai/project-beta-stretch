import React from "react";
import moment from "moment";

class AppointmentsHistory extends React.Component {
  state = {
    appointments: [],
    vinSearch: "",
  };

  handleVINChange = (event) => {
    const name = event.target.name;
    const value = event.target.value;
    this.setState({
      [name]: value,
    });
  };

  handleSearch = async (event) => {
    event.preventDefault();
    const url = "http://localhost:8080/api/appointments/";
    const response = await fetch(url);
    if (response.ok) {
      const data = await response.json();
      const appointments = data.appointments.filter(
        (appointment) => appointment.vin === this.state.vinSearch
      );
      this.setState({ appointments: appointments, vinSearch: "" });
    }
  };

  render() {
    return (
      <div>
        <h1 className="header mt-4">Appointments History</h1>
        <div className="row">
          <div className="col-6">
            <div className="shadow p-4 mt-4">
              <form onSubmit={this.handleSearch} id="search-vin-form">
                <div className="form-floating mb-3">
                  <input
                    onChange={this.handleVINChange}
                    placeholder="Search VIN Number"
                    type="text"
                    name="vinSearch"
                    id="vinSearch"
                    value={this.state.vinSearch}
                    className="form-control"
                  />
                  <label htmlFor="name">Search VIN Number</label>
                </div>
                <button
                  className="btn btn-primary"
                  style={{ color: "black", backgroundColor: "white" }}
                >
                  Search
                </button>
              </form>
            </div>
          </div>
        </div>
        <table className="table mt-4">
          <thead>
            <tr>
              <th>VIN</th>
              <th>Customer Name</th>
              <th>Date</th>
              <th>Time</th>
              <th>Technician</th>
              <th>Reason</th>
            </tr>
          </thead>
          <tbody>
            {this.state.appointments.map((appointment) => {
              return (
                <tr key={appointment.id} className="finish">
                  <td>{appointment.vin}</td>
                  <td>{appointment.name}</td>
                  <td>{appointment.date}</td>
                  <td>
                    {moment(appointment.time, "HH:mm:ss").format("h:mm a")}
                  </td>
                  <td>{appointment.technician.name}</td>
                  <td>{appointment.reason}</td>
                </tr>
              );
            })}
          </tbody>
        </table>
      </div>
    );
  }
}
export default AppointmentsHistory;
