import React from "react";

class AppointmentForm extends React.Component {
  state = {
    vin: "",
    name: "",
    date: "",
    time: "",
    reason: "",
    technicians: [],
  };

  handleSubmit = async (event) => {
    event.preventDefault();
    const data = { ...this.state };
    delete data.technicians;
    const appointmentUrl = "http://localhost:8080/api/appointments/";
    const fetchConfig = {
      method: "post",
      body: JSON.stringify(data),
      headers: {
        "Content-Type": "application/json",
      },
    };
    const response = await fetch(appointmentUrl, fetchConfig);
    if (response.ok) {
      await response.json();
      const cleared = {
        vin: "",
        name: "",
        date: "",
        time: "",
        reason: "",
        technician: "",
      };
      this.setState(cleared);
    }
  };

  handleInputChange = (event) => {
    const name = event.target.name;
    const value = event.target.value;
    this.setState({
      [name]: value,
    });
  };

  async componentDidMount() {
    const url = "http://localhost:8080/api/technicians/";
    const response = await fetch(url);
    if (response.ok) {
      const data = await response.json();
      this.setState({ technicians: data.technicians });
    }
  }

  render() {
    return (
      <div className="row">
        <div className="offset-3 col-6">
          <div className="shadow p-4 mt-4">
            <h1>Make an Appointment</h1>
            <form onSubmit={this.handleSubmit} id="create-technician-form">
              <div className="form-floating mb-3">
                <input
                  onChange={this.handleInputChange}
                  placeholder="VIN Number"
                  type="text"
                  name="vin"
                  id="vin"
                  value={this.state.vin}
                  className="form-control"
                />
                <label htmlFor="name">VIN Number</label>
              </div>
              <div className="form-floating mb-3">
                <input
                  onChange={this.handleInputChange}
                  placeholder="Name"
                  type="text"
                  name="name"
                  id="name"
                  value={this.state.name}
                  className="form-control"
                />
                <label htmlFor="vin">Name</label>
              </div>
              <div className="form-floating mb-3">
                <input
                  onChange={this.handleInputChange}
                  placeholder="Date"
                  type="date"
                  name="date"
                  id="date"
                  value={this.state.date}
                  className="form-control"
                />
                <label htmlFor="date">Date</label>
              </div>
              <div className="form-floating mb-3">
                <input
                  onChange={this.handleInputChange}
                  placeholder="Time"
                  type="time"
                  name="time"
                  id="time"
                  value={this.state.time}
                  className="form-control"
                />
                <label htmlFor="time">Time</label>
              </div>
              <div className="mb-3">
                <textarea
                  onChange={this.handleInputChange}
                  placeholder="Reason"
                  name="reason"
                  id="reason"
                  rows="3"
                  value={this.state.reason}
                  className="form-control"
                />
              </div>
              <div className="mb-3">
                <select
                  onChange={this.handleInputChange}
                  name="technician"
                  id="technician"
                  value={this.state.technician}
                  className="form-select"
                >
                  <option value="">Choose a technician</option>
                  {this.state.technicians.map((technician) => {
                    return (
                      <option key={technician.id} value={technician.id}>
                        {technician.name}
                      </option>
                    );
                  })}
                </select>
              </div>
              <button className="btn btn-primary">Create</button>
            </form>
          </div>
        </div>
      </div>
    );
  }
}
export default AppointmentForm;
