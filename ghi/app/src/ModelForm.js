import React from "react";

class ModelForm extends React.Component {
  state = {
    name: "",
    pictureUrl: "",
    manufacturer: "",
    manufacturers: [],
  };

  handleSubmit = async (event) => {
    event.preventDefault();
    const data = { ...this.state };
    delete data.manufacturers;
    const modelUrl = "http://localhost:8100/api/models/";
    const fetchConfig = {
      method: "post",
      body: JSON.stringify({
        name: data.name,
        picture_url: data.pictureUrl,
        manufacturer_id: data.manufacturer,
      }),
      headers: {
        "Content-Type": "application/json",
      },
    };
    const response = await fetch(modelUrl, fetchConfig);
    if (response.ok) {
      await response.json();
      const cleared = {
        name: "",
        pictureUrl: "",
        manufacturer: "",
      };
      this.setState(cleared);
    }
  };

  handleInputChange = (event) => {
    const name = event.target.name;
    const value = event.target.value;
    this.setState({
      [name]: value,
    });
  };

  async componentDidMount() {
    const url = "http://localhost:8100/api/manufacturers/";
    const response = await fetch(url);
    if (response.ok) {
      const data = await response.json();
      this.setState({ manufacturers: data.manufacturers });
    }
  }

  render() {
    return (
      <div className="row">
        <div className="offset-3 col-6">
          <div className="shadow p-4 mt-4">
            <h1>Add a Vehicle Model</h1>
            <form onSubmit={this.handleSubmit} id="create-model-form">
              <div className="form-floating mb-3">
                <input
                  onChange={this.handleInputChange}
                  placeholder="Name"
                  type="text"
                  name="name"
                  id="name"
                  value={this.state.name}
                  className="form-control"
                />
                <label htmlFor="name">Name</label>
              </div>
              <div className="form-floating mb-3">
                <input
                  onChange={this.handleInputChange}
                  placeholder="Picture Url"
                  type="text"
                  name="pictureUrl"
                  id="pictureUrl"
                  value={this.state.pictureUrl}
                  className="form-control"
                />
                <label htmlFor="vin">Picture Url</label>
              </div>
              <div className="mb-3">
                <select
                  onChange={this.handleInputChange}
                  name="manufacturer"
                  id="manufacturer"
                  value={this.state.manufacturer}
                  className="form-select"
                >
                  <option value="">Choose a Manufacturer</option>
                  {this.state.manufacturers?.map((manufacturer) => {
                    return (
                      <option key={manufacturer.id} value={manufacturer.id}>
                        {manufacturer.name}
                      </option>
                    );
                  })}
                </select>
              </div>
              <button className="btn btn-primary">Create</button>
            </form>
          </div>
        </div>
      </div>
    );
  }
}
export default ModelForm;
