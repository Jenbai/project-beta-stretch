import React from "react";

class ModelsList extends React.Component {
  state = {
    models: [],
  };

  async componentDidMount() {
    const response = await fetch("http://localhost:8100/api/models/");
    if (response.ok) {
      const data = await response.json();
      this.setState({ models: data.models });
    }
  }

  render() {
    return (
      <div>
        <h1 className="header mt-4"> Vehicle Models List</h1>
        <table className="table table-striped">
          <thead>
            <tr>
              <th>Manufacturer</th>
              <th>Model</th>
              <th>Images</th>
            </tr>
          </thead>
          <tbody>
            {this.state.models?.map((model) => {
              return (
                <tr key={model.id}>
                  <td>{model.manufacturer.name}</td>
                  <td>{model.name}</td>
                  <td>
                    <img
                      src={model.picture_url}
                      alt={model.name}
                      width="300"
                      height="200"
                    />
                  </td>
                </tr>
              );
            })}
          </tbody>
        </table>
      </div>
    );
  }
}

export default ModelsList;
