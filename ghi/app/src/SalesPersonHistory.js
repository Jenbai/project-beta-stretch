import React from "react";

class SalesPersonHistory extends React.Component {
  state = {
    employees: [],
    sales: [],
  };

  handleChange = async (e) => {
    const name = e.target.name;
    const value = e.target.value;
    this.setState({ [name]: value });

    const responseSalesHistory = await fetch(
      `http://localhost:8090/api/employees/sales/${value}`
    );
    if (responseSalesHistory.ok) {
      const data = await responseSalesHistory.json();
      const salesHistory = data["sales"];
      for (let n = 0; n < salesHistory.length; n++) {
        salesHistory[n]["employee"] = salesHistory[n]["employee_name"];
      }
      this.setState({ sales: salesHistory });
    }
  };

  async componentDidMount() {
    const response = await fetch("http://localhost:8090/api/employees/");
    if (response.ok) {
      const data = await response.json();
      this.setState({ employees: data.employees });
    }
  }

  render() {
    return (
      <div>
        <h1 className="header mt-4">Sales History</h1>
        <form>
          <div className="mb-3">
            <select
              onChange={this.handleChange}
              name="employeeName"
              id="employees"
              value={this.state.employee}
              className="form-select"
              aria-label="Default select example"
            >
              <option>Choose an Employee</option>
              {this.state.employees?.map((employee) => {
                return (
                  <option
                    key={employee.id}
                    value={employee.id}
                    id={employee.id}
                  >
                    {employee.employee_name} / {employee.employee_number}
                  </option>
                );
              })}
            </select>
          </div>
        </form>
        <table className="table table-striped">
          <thead>
            <tr>
              <th>Sales Person</th>
              <th>Customer</th>
              <th>VIN</th>
              <th>Sale price</th>
            </tr>
          </thead>
          <tbody>
            {this.state.sales?.map((sale) => {
              return (
                <tr key={sale.id}>
                  <td>{sale.employee}</td>
                  <td>{sale.customer}</td>
                  <td>{sale.vin}</td>
                  <td>${sale.price}.00</td>
                </tr>
              );
            })}
          </tbody>
        </table>
      </div>
    );
  }
}

export default SalesPersonHistory;
