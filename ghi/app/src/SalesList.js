import React from "react";

class SalesList extends React.Component {
  state = {
    sales: [],
  };

  async componentDidMount() {
    const response = await fetch("http://localhost:8090/api/sales/");
    if (response.ok) {
      const data = await response.json();
      this.setState({ sales: data.sales });
    }
  }

  render() {
    return (
      <div>
        <h1 className="header mt-4">Sales List</h1>
        <table className="table table-striped">
          <thead>
            <tr>
              <th>Employee</th>
              <th>Employee Number</th>
              <th>Customer</th>
              <th>Automobile VIN</th>
              <th>Price</th>
            </tr>
          </thead>
          <tbody>
            {this.state.sales?.map((sale) => {
              return (
                <tr key={sale.id}>
                  <td>{sale.employee_name}</td>
                  <td>{sale.employee_number}</td>
                  <td>{sale.customer}</td>
                  <td>{sale.vin}</td>
                  <td>${sale.price}.00</td>
                </tr>
              );
            })}
          </tbody>
        </table>
      </div>
    );
  }
}

export default SalesList;
